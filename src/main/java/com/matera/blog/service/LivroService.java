package com.matera.blog.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.matera.blog.model.Livro;
import com.matera.blog.repository.LivroRepository;

@Service
public class LivroService {

	@Autowired
	private LivroRepository repository;
	
	//Retorna uma lista com todos posts inseridos
		public List<Livro> findAll() {
			return repository.findAll(); 
		}
		
		//Retorno um post a partir do ID
		public Livro findOne(Long id) {
			return repository.findOne(id);
		}
		
		//Salva ou atualiza um post
		public Livro save(Livro livro) {
			return repository.saveAndFlush(livro);
		}
		
		//Exclui um post
		public void delete(Long id) {
			repository.delete(id);
		}
}
